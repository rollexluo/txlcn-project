package com.cluo.txlcn.controller;

import com.cluo.txlcn.service.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    @PostMapping("/save")
    public String saveOrderInfo(){
        boolean flag = orderInfoService.saveOrderInfo();
        if(flag){
            return "success";
        }
        return "failure";
    }
}
