package com.cluo.txlcn.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@Service
@FeignClient(value = "txlcn-store")
public interface StoreInfoService {

    @PostMapping("/store/save")
    public boolean saveStoreInfo();
}
