package com.cluo.txlcn.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("order_info")
public class OrderInfo {

    @TableId
    private Long id;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "order_num")
    private Long orderNum;

    @TableField(value = "order_amt")
    private BigDecimal orderAmt;

    @TableField(value = "rmrk")
    private String rmrk;
}
