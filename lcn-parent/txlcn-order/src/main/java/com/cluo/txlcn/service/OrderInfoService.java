package com.cluo.txlcn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cluo.txlcn.entity.OrderInfo;

public interface OrderInfoService extends IService<OrderInfo> {

    public boolean saveOrderInfo();
}
