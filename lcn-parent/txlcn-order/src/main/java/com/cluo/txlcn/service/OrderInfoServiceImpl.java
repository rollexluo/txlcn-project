package com.cluo.txlcn.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cluo.txlcn.dao.OrderInfoMapper;
import com.cluo.txlcn.entity.OrderInfo;
import com.cluo.txlcn.feign.StoreInfoService;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService{

    @Autowired
    private StoreInfoService storeInfoService;

    @LcnTransaction
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public boolean saveOrderInfo(){
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderAmt(new BigDecimal(10000));
        orderInfo.setOrderNum(100L);
        orderInfo.setProductName("iphone12");
        orderInfo.setRmrk("质量好，又实惠");
        boolean flag = this.save(orderInfo);
        boolean flag2 = storeInfoService.saveStoreInfo();
        if(flag && flag2){
            return true;
        }
        return false;
    }
}
