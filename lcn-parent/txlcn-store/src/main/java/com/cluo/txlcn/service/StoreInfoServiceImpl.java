package com.cluo.txlcn.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cluo.txlcn.dao.StoreInfoMapper;
import com.cluo.txlcn.entity.StoreInfo;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreInfoServiceImpl extends ServiceImpl<StoreInfoMapper, StoreInfo> implements StoreInfoService{

    @LcnTransaction
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public boolean saveStoreInfo(StoreInfo storeInfo){
        //int i = 10/0;  模拟异常
        return this.save(storeInfo);
    }
}
