package com.cluo.txlcn.controller;

import com.cluo.txlcn.entity.StoreInfo;
import com.cluo.txlcn.service.StoreInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/store")
public class StoreInfoController {

    @Autowired
    private StoreInfoService storeInfoService;

    @PostMapping("/save")
    public boolean saveStoreInfo(){
        StoreInfo storeInfo = new StoreInfo();
        storeInfo.setProductName("iphone12");
        storeInfo.setStoreNum(100L);
        storeInfo.setRmrk("好看又实用");
        return storeInfoService.saveStoreInfo(storeInfo);
    }
}
