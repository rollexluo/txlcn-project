package com.cluo.txlcn.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cluo.txlcn.entity.StoreInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StoreInfoMapper extends BaseMapper<StoreInfo> {
}
