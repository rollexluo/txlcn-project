package com.cluo.txlcn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cluo.txlcn.entity.StoreInfo;

public interface StoreInfoService extends IService<StoreInfo> {

    public boolean saveStoreInfo(StoreInfo storeInfo);
}
