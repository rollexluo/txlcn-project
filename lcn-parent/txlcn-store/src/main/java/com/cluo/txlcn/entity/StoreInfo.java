package com.cluo.txlcn.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("store_info")
public class StoreInfo {

    @TableId
    private Long id;

    @TableField(value = "product_name")
    private String productName;

    @TableField(value = "store_num")
    private Long storeNum;

    @TableField(value = "rmrk")
    private String rmrk;
}
